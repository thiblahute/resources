## gstreamer (core)

### Manual

| scope     | function     | arg.     | M | ret | comment |
|:--------- |:------------ |:-------- |:-:|:---:| ------- |
| BufferRef | pts          |          |   |  O  |  |
| BufferRef | set_pts      | pts      | O |     | allows unsetting |
| BufferRef | dts          |          |   |  O  |  |
| BufferRef | set_dts      | dts      | O |     | allows unsetting |
| BufferRef | dts_or_pts   |          |   |  O  |  |
| BufferRef | duration     |          |   |  O  |         |
| BufferRef | set_duration | duration | O |     | allows unsetting |

| scope | function            | arg.    | M | ret | comment |
|:----- |:------------------- |:------- |:-:|:---:| ------- |
| Bus   | iter_timed          | timeout | O |     | None to wait forever |
| Bus   | iter_timed_filtered | timeout | O |     | None to wait forever |
| Bus   | timed_pop_filtered  | timeout | O |     | None to wait forever |

| scope           | function                  | arg.            | M | ret | comment |
|:--------------- |:------------------------- |:--------------- |:-:|:---:| ------- |
| ClockId         | time                      |                 |   |  M  |  |
| PeriodicClockId | interval                  |                 |   |  M  |  |
| Clock           | adjust_with_calibration   | internal_target | M |  M  |  |
|                 |                           | cinternal       | M |     |  |
|                 |                           | cexternal       | M |     |         |
|                 |                           | cnum            | M |     |         |
|                 |                           | cdenom          | M |     |         |
| Clock           | unadjust_with_calibration | external_target | M |  M  |  |
|                 |                           | cinternal       | M |     |  |
|                 |                           | cexternal       | M |     |         |
|                 |                           | cnum            | M |     |         |
|                 |                           | cdenom          | M |     |         |
| ClockExtManual  | new_periodic_id           | start_time      | M |     | arg checked in C |
|                 |                           | interval        | M |     | arg checked in C |
| ClockExtManual  | periodic_id_reinit        | start_time      | M |     | arg not checked in C |
|                 |                           | interval        | M |     | arg not checked in C |
| ClockExtManual  | new_single_shot_id        | time            | M |     | arg not checked in C |
| ClockExtManual  | single_shot_id_reinit     | time            | M |     | arg not checked in C |

| scope                   | function      | arg.      | M | ret | comment |
|:----------------------- |:------------- |:--------- |:-:|:---:| ------- |
| ControlBindingExtManual | g_value_array | timestamp | M |     | arg checked in C |
|                         |               | interval  | M |     | arg checked in C |

| scope                  | function    | arg.      | M | ret | comment |
|:---------------------- |:----------- |:--------- |:-:|:---:| ------- |
| ControlSourceExtManual | value_array | timestamp | M |     | arg not checked in C |
|                        |             | interval  | M |     | arg not checked in C |

| scope            | function             | arg. | M | ret | comment |
|:---------------- |:-------------------- |:---- |:-:|:---:| ------- |
| ElementExtManual | current_running_time |      |   |  O  | doc: ret is Opt |
| ElementExtManual | current_clock_time   |      |   |  O  | doc: ret is Opt |

| scope (event)  | function           | arg.      | M | ret | comment |
|:-------------- |:------------------ |:--------- |:-:|:---:| ------- |
| Gap            | new                | timestamp | M |     | arg checked in C |
|                |                    | duration  | O |     | arg not checked in C |
| Gap            | builder            | timestamp | M |     | same as ^ |
|                |                    | duration  | O |     | same as ^ |
| Gap            | get                |           |   |  T  | timestamp: M, duration: O |
| Qos            | new                | timestamp | O |     | arg not checked in C |
| Qos            | builder            | timestamp | O |     | same as ^ |
| Qos            | get                |           |   |  T  | ret: timestamp, same as ^ |
| Seek           | trickmode_interval |           |   |  O  |         |
| Latency        | new                | latency   | M |     |  |
| Latency        | builder            | latency   | M |     | same as ^ |
| Latency        | latency            |           |   |  M  |  |
| GapBuilder     | new                | timestamp | M |     | same as ^ |
|                |                    | duration  | O |     | same as ^ |
| QosBuilder     | new                | timestamp | O |     | same as ^ |
| LatencyBuilder | new                | latency   | M |     | same as ^ |

| scope              | function      | arg.      | M | ret | comment |
|:------------------ |:------------- |:--------- |:-:|:---:| ------- |
| GstObjectExtManual | g_value_array | timestamp | M |     | arg checked in C |
|                    |               | interval  | M |     | arg checked in C |

| scope (message)  | function     | arg.         | M | ret | comment |
|:---------------- |:------------ |:------------ |:-:|:---:| ------- |
| AsyncDone        | new          | running_time | O |     | doc: arg: None serves a special purpose |
| AsyncDone        | builder      | running_time | O |     | same as ^ |
| AsyncDone        | running_time |              |   |  O  | same as ^ |
| Qos              | new          | running_time | O |     | doc: arg: None when unknown |
|                  |              | stream_time  | O |     | same as ^ |
|                  |              | timestamp    | O |     | same as ^ |
|                  |              | duration     | O |     | same as ^ |
| Qos              | builder      | running_time | O |     | same as ^ |
|                  |              | stream_time  | O |     | same as ^ |
|                  |              | timestamp    | O |     | same as ^ |
|                  |              | duration     | O |     | same as ^ |
| Qos              | get          |              |   |  T  | same as ^ |
| ResetTime        | new          | running_time | M |     | arg not checked in C |
| ResetTime        | builder      | running_time | M |     | same as ^ |
| ResetTime        | running_time |              |   |  M  |         |
| AsyncDoneBuilder | new          | running_time | O |     | same as ^ |
| QosBuilder       | new          | running_time | O |     | same as ^ |
|                  |              | stream_time  | O |     | same as ^ |
|                  |              | timestamp    | O |     | same as ^ |
|                  |              | duration     | O |     | same as ^ |
| ResetTimeBuilder | new          | running_time | M |     | same as ^ |

| scope (meta)           | function  | arg.      | M | ret | comment |
|:---------------------- |:--------- |:--------- |:-:|:---:| ------- |
| ReferenceTimestampMeta | add       | timestamp | M |     | arg checked in C |
|                        |           | duration  | O |     | doc: arg is opt |
| ReferenceTimestampMeta | timestamp |           |   |  O  | None after init |
| ReferenceTimestampMeta | duration  |           |   |  O  | None after init |

| scope (query) | function | arg. | M | ret | comment |
|:------------- |:-------- |:---- |:-:|:---:| ------- |
| Latency       | result   |      |   |  T  | ret: min: M (init at 0), max: O |
| Latency       | set      | min  | M |     | arg checked in C |
|               |          | max  | O |     | arg not checked in C |

### Auto

| scope | function  | arg.    | M | ret | comment |
|:----- |:--------- |:------- |:-:|:---:| ------- |
| Bus   | timed_pop | timeout | O |     | doc: when None, wait forever |

| scope    | function                  | arg.       | M | ret | comment |
|:-------- |:------------------------- |:---------- |:-:|:---:| ------- |
| ClockExt | add_observation           | slave      | M |     | arg not checked in C |
|          |                           | master     | M |     | arg not checked in C |
| ClockExt | add_observation_unapplied | slave      | M |  M  | ret: (internal, external, rate_num, rate_denom) |
|          |                           | master     | M |     | args not checked in C |
| ClockExt | adjust_unlocked           | internal   | M |  O  | arg not checked in C |
| ClockExt | calibration               |            |   |  M  | ret: (internal, external, rate_num, rate_denom) |
| ClockExt | internal_time             |            |   |  M  | doc: ret is opt |
| ClockExt | resolution                |            |   |  M  |         |
| ClockExt | time                      |            |   |  O  | doc: ret is opt |
| ClockExt | timeout                   |            |   |  O  |         |
| ClockExt | set_calibration           | internal   | M |     | arg not checked in C |
|          |                           | external   | M |     | arg not checked in C |
|          |                           | rate_num   | M |     | arg checked in C |
|          |                           | rate_denom | M |     | arg checked in C |
| ClockExt | set_resolution            | resolution | M |  M  | args not checked in C |
| ClockExt | set_timeout               | timeout    | O |     | arg not checked in C |
| ClockExt | unadjust_unlocked         | external   | O |  O  | arg not checked in C |
| ClockExt | wait_for_sync             | timeout    | O |     | doc: when None, wait forever |

| scope             | function    | arg.      | M | ret | comment |
|:----------------- |:----------- |:--------- |:-:|:---:| ------- |
| ControlBindingExt | value       | timestamp | M |     | arg checked in C |
| ControlBindingExt | sync_values | timestamp | M |     | arg not checked in C |
|                   |             | last_sync | O |     | arg not checked in C |

| scope            | function  | arg.      | M | ret | comment |
|:---------------- |:--------- |:--------- |:-:|:---:| ------- |
| ControlSourceExt | get_value | timestamp | M |     |  |

| scope      | function       | arg.    | M | ret | comment |
|:---------- |:-------------- |:------- |:-:|:---:| ------- |
| ElementExt | base_time      |         |   |  O  |  |
| ElementExt | start_time     |         |   |  O  |  |
| ElementExt | state          | timeout | O |     | doc: when None, wait forever |
| ElementExt | set_base_time  | time    | M |     | arg not checked in C |
| ElementExt | set_start_time | time    | O |     | doc: special purpose when None |

| scope        | function          | arg.         | M | ret | comment |
|:------------ |:----------------- |:------------ |:-:|:---:| ------- |
| GstObjectExt | control_rate      |              |   |  O  | doc: ret is opt |
| GstObjectExt | value             | timestamp    | M |     | arg checked in C |
| GstObjectExt | set_control_rate  | control_rate | O |     | arg not checked in C |
| GstObjectExt | suggest_next_sync |              |   |  O  | doc: ret is opt |
| GstObjectExt | sync_values       | timestamp    | M |     | arg checked in C |

| scope       | function    | arg.    | M | ret | comment |
|:----------- |:----------- |:------- |:-:|:---:| ------- |
| PipelineExt | delay       |         |   |  M  |         |
| PipelineExt | latency     |         |   |  O  | doc: ret can be None |
| PipelineExt | set_delay   | delay   | M |     | arg checked in C |
| PipelineExt | set_latency | latency | O |     | doc: arg can be None |

### Subclass

| scope        | function                 | arg.           | M | ret | comment |
|:------------ |:------------------------ |:-------------- |:-:|:---:| ------- |
| ClockImpl    | change_resolution        | old_resolution | M |  M  |         |
|              |                          | new_resolution | M |     |         |
| ClockImpl    | resolution               |                |   |  M  |         |
| ClockImpl    | internal_time            |                |   |  M  | doc: ret is opt but this is misleading |
| ClockImplExt | parent_change_resolution | old_resolution | M |  M  | same as ^ |
|              |                          | new_resolution | M |     | same as ^ |
| ClockImplExt | parent_resolution        |                |   |  M  | same as ^ |
| ClockImplExt | parent_internal_time     |                |   |  M  | same as ^ |
